package com.company.carsaleapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarSaleApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarSaleApiApplication.class, args);
	}

}
