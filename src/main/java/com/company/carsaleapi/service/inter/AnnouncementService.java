package com.company.carsaleapi.service.inter;

import com.company.carsaleapi.dto.AnnouncementDto;
import com.company.carsaleapi.dto.AnnouncementSearchDto;
import com.company.carsaleapi.dto.UserAnnouncementsDto;

import java.util.List;



public interface AnnouncementService {

    AnnouncementDto getAnnouncementById(Long id) throws Exception;
    List<AnnouncementDto> getAllAnnouncementByPage(int page, int size);

    AnnouncementDto updateAnnouncement(AnnouncementDto announcement);

    AnnouncementDto createAnnouncement(AnnouncementDto announcement);

    List<UserAnnouncementsDto> getAnnouncementByUser(Long user,Integer page,Integer size);

    List<AnnouncementDto> getSearchedAnnouncements(Integer page, Integer size, AnnouncementSearchDto dto);
    boolean deleteAnnouncementById(Long id);

}
