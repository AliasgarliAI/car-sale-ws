package com.company.carsaleapi.service.impl;

import com.company.carsaleapi.dto.UserWithAnnouncementsDto;
import com.company.carsaleapi.entity.User;
import com.company.carsaleapi.exception.NotFoundException;
import com.company.carsaleapi.mapper.UserMapper;
import com.company.carsaleapi.repository.UserRepository;
import com.company.carsaleapi.service.inter.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

//    @Transactional
//    @Override
//    public UserWithAnnouncementsDto findUserAnnouncements(Long userId, Integer page) throws NotFoundException {
//
//        return userRepository.findUserAnnouncements(userId, page,3);
//
//
//
//    }
}
