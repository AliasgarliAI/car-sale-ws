package com.company.carsaleapi.service.impl;

import com.company.carsaleapi.dto.AnnouncementDto;
import com.company.carsaleapi.dto.AnnouncementSearchDto;
import com.company.carsaleapi.dto.UserAnnouncementsDto;
import com.company.carsaleapi.entity.Announcement;
import com.company.carsaleapi.exception.NotFoundException;
import com.company.carsaleapi.mapper.AnnouncementMapper;
import com.company.carsaleapi.mapper.UserMapper;
import com.company.carsaleapi.repository.AnnouncementRepository;
import com.company.carsaleapi.repository.specification.AnnouncementSearchSpecification;
import com.company.carsaleapi.service.inter.AnnouncementService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class AnnouncementServiceImpl implements AnnouncementService {

    private final AnnouncementRepository announcementRepository;
    private final AnnouncementMapper mapper;
    private final UserMapper userMapper;

    @Override
    @Transactional
    public AnnouncementDto getAnnouncementById(Long id) throws Exception {
        Announcement announcement = announcementRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Announcement was not found"));

        return mapper.toDto(announcement);
    }

    @Override
    @Transactional
    public List<AnnouncementDto> getAllAnnouncementByPage(int page, int size) {
        Page<Announcement> announcements = announcementRepository.findAll(PageRequest.of(page, size));

        return announcements.stream()
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public AnnouncementDto updateAnnouncement(AnnouncementDto dto) {
        Announcement announcement = announcementRepository.save(mapper.toEntity(dto));
        return mapper.toDto(announcement);
    }

    @Override
    @Transactional
    public AnnouncementDto createAnnouncement(AnnouncementDto dto) {
        log.info("this is dto object ---->> {}", dto);
        Announcement entity = mapper.toEntity(dto);
        entity.getAnnouncementDetails().setBrand(entity.getAnnouncementDetails().getCarModel().getBrand());
        log.info("this is entity object ---->> {}", entity);
        Announcement announcement = announcementRepository.save(entity);
        return mapper.toDto(announcement);
    }

    @Transactional
    @Override
    public List<UserAnnouncementsDto> getAnnouncementByUser(Long user, Integer page, Integer size) {
        List<Announcement> announcement = announcementRepository.getAnnouncementByUserId(user, PageRequest.of(page, size));
        return userMapper.listEntityToDtoList(announcement);
    }

    @Override
    @Transactional
    public List<AnnouncementDto> getSearchedAnnouncements(Integer page, Integer size, AnnouncementSearchDto dto) {
        Specification<Announcement> specification = announcementRepository.createSpecification(dto);

        return announcementRepository.findAll(specification, PageRequest.of(page, size))
                .stream().map(mapper::toDto).toList();
    }

    @Override
    public boolean deleteAnnouncementById(Long id) {
        announcementRepository.deleteById(id);
        return true;
    }
}
