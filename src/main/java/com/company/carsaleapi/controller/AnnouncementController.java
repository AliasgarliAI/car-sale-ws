package com.company.carsaleapi.controller;

import com.company.carsaleapi.dto.AnnouncementDto;
import com.company.carsaleapi.dto.AnnouncementSearchDto;
import com.company.carsaleapi.dto.UserAnnouncementsDto;
import com.company.carsaleapi.service.inter.AnnouncementService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Max;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/elan")
@RequiredArgsConstructor
@Slf4j
public class AnnouncementController {

    private final AnnouncementService service;

    @GetMapping("/{id}")
    public AnnouncementDto getAnnouncementById(@PathVariable Long id) throws Exception {

        return service.getAnnouncementById(id);
    }

    @GetMapping
    public List<AnnouncementDto> getAnnouncements(@RequestParam int page, @RequestParam int size) {
        return service.getAllAnnouncementByPage(page, size);
    }

    @GetMapping("/user/{userId}")
    public List<UserAnnouncementsDto> getAnnouncementsByUser(
            @Valid
            @PathVariable Long userId,
            @RequestParam("page") Integer page,
            @Max(value = 50) @RequestParam("size") Integer size) {
        return service.getAnnouncementByUser(userId, page, size);
    }

    @PostMapping("/search")
    public List<AnnouncementDto> getSearchedAnnouncements(
            @Valid
            @RequestParam("page") Integer page,
            @Max(value = 50) @RequestParam("size") Integer size,
            @RequestBody AnnouncementSearchDto searchDto) {
        return service.getSearchedAnnouncements(page,size,searchDto);
    }

    @PostMapping
    public AnnouncementDto createAnnouncement(@RequestBody AnnouncementDto announcementDto) {
        log.info("dto model - > {} ", announcementDto);
        return service.createAnnouncement(announcementDto);
    }

}
