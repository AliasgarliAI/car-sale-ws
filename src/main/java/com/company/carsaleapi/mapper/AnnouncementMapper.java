package com.company.carsaleapi.mapper;

import com.company.carsaleapi.dto.AnnouncementDetailsDto;
import com.company.carsaleapi.dto.AnnouncementDto;
import com.company.carsaleapi.dto.BrandDto;
import com.company.carsaleapi.dto.CityDto;
import com.company.carsaleapi.dto.ColorDto;
import com.company.carsaleapi.dto.ConditionDto;
import com.company.carsaleapi.dto.ModelDto;
import com.company.carsaleapi.dto.UserDto;
import com.company.carsaleapi.dto.VehicleSupplyDto;
import com.company.carsaleapi.entity.Announcement;
import com.company.carsaleapi.entity.AnnouncementDetails;
import com.company.carsaleapi.entity.Brand;
import com.company.carsaleapi.entity.City;
import com.company.carsaleapi.entity.Color;
import com.company.carsaleapi.entity.Condition;
import com.company.carsaleapi.entity.Model;
import com.company.carsaleapi.entity.User;
import com.company.carsaleapi.entity.VehicleSupply;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface AnnouncementMapper {


    @Mapping(target = "announcementDetails", source = "announcementDetails")
    AnnouncementDto toDto(Announcement entity);

    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "announcementDetails", source = "announcementDetails")
    Announcement toEntity(AnnouncementDto dto);


    @Mapping(target = "isLoanAccepted", source = "loanAccepted")
    AnnouncementDetailsDto toDto(AnnouncementDetails entity);

    @Mapping(target = "brand", ignore = true)
    AnnouncementDetails toEntity(AnnouncementDetailsDto dto);

    CityDto toDto(City entity);

    City toEntity(CityDto dto);

    BrandDto toDto(Brand entity);


    Brand toEntity(BrandDto dto);

    ModelDto toDto(Model entity);

    Model toEntity(ModelDto dto);


    VehicleSupplyDto toDto(VehicleSupply entity);

    VehicleSupply toEntity(VehicleSupplyDto dto);

    ColorDto toDto(Color entity);

    Color toEntity(ColorDto dto);


    ConditionDto toDto(Condition entity);


    Condition toEntity(ConditionDto dto);

    UserDto toDto(User entity);



    @Mapping(target = "announcements", ignore = true)
    User toEntity(UserDto dto);

}
