package com.company.carsaleapi.mapper;

import com.company.carsaleapi.dto.UserAnnouncementsDto;
import com.company.carsaleapi.dto.UserWithAnnouncementsDto;
import com.company.carsaleapi.entity.Announcement;
import com.company.carsaleapi.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserWithAnnouncementsDto entityToDto(User user);

    @Mapping(target = "announcementId",source = "id")
    @Mapping(target = "price",source = "announcementDetails.price")
    @Mapping(target = "currency",source = "announcementDetails.priceCurrency")
    UserAnnouncementsDto entityToDto(Announcement entity);

    List<UserAnnouncementsDto> listEntityToDtoList(List<Announcement> entities);

}
