package com.company.carsaleapi.repository.specification;

public enum ComparisonOperator {

    EQUAL,
    GREATER_THAN,
    LESS_THAN,
    BETWEEN
}
