package com.company.carsaleapi.repository.specification;

import com.company.carsaleapi.dto.AnnouncementSearchDto;
import com.company.carsaleapi.entity.Announcement;
import com.company.carsaleapi.entity.AnnouncementDetails;
import com.company.carsaleapi.entity.City;
import com.company.carsaleapi.entity.Color;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

import static com.company.carsaleapi.repository.specification.CommonSpecification.*;
import static com.company.carsaleapi.repository.specification.ComparisonOperator.*;


public interface AnnouncementSearchSpecification {


    default Specification<Announcement> createSpecification(AnnouncementSearchDto dto) {
        List<Predicate> predicates = new ArrayList<>();
        return ((root, query, criteriaBuilder) -> {

            Join<Object, Object> announcementDetails = root.join( "announcementDetails",JoinType.LEFT);

            addEqualPredicateIfPresent(dto.getGear(),
                    announcementDetails
                            .get(AnnouncementDetails.Fields.gearType), criteriaBuilder, predicates);

            addEqualPredicateIfPresent(dto.getGearBox(),
                    announcementDetails
                            .get(AnnouncementDetails.Fields.gearBoxType), criteriaBuilder, predicates);

            addEqualPredicateIfPresent(dto.getFuelType(),
                    announcementDetails
                            .get(AnnouncementDetails.Fields.fuelType),criteriaBuilder,predicates);

            if (!ObjectUtils.isEmpty(dto.getCity()))
                addLongPredicateIfPresent(dto.getCity().getId(), null, EQUAL,
                        announcementDetails.get(AnnouncementDetails.Fields.city)
                                .get(City.Fields.id), criteriaBuilder, predicates);

            if (!ObjectUtils.isEmpty(dto.getColor()))
                addLongPredicateIfPresent(dto.getColor().getId(), null, EQUAL,
                        announcementDetails.get(AnnouncementDetails.Fields.color)
                                .get(Color.Fields.id), criteriaBuilder, predicates);

            var comparisonType = getComparisonType(dto.getManufactureDateMin(), dto.getManufactureDateMax());
            addDatePredicateIfPresent(dto.getManufactureDateMin(), dto.getManufactureDateMax(), comparisonType,
                    announcementDetails
                            .get(AnnouncementDetails.Fields.manufactureDate), criteriaBuilder, predicates);

            comparisonType = getComparisonType(dto.getPriceMin(), dto.getPriceMax());
            addBigDecimalPredicateIfPresent(dto.getPriceMin(), dto.getPriceMax(), comparisonType,
                    announcementDetails
                            .get(AnnouncementDetails.Fields.price), criteriaBuilder, predicates);

            if (!ObjectUtils.isEmpty(dto.getHasBarter()) && dto.getHasBarter()) {
                addIsTruePredicateIfPresent(announcementDetails
                        .get(AnnouncementDetails.Fields.hasBarter), criteriaBuilder, predicates);
            }
            if (!ObjectUtils.isEmpty(dto.getHasBarter()) && !dto.getHasBarter()) {
                addIsFalsePredicateIfPresent(announcementDetails
                        .get(AnnouncementDetails.Fields.hasBarter), criteriaBuilder, predicates);
            }

            if (!ObjectUtils.isEmpty(dto.getHasAccident()) && dto.getHasAccident()) {
                addIsTruePredicateIfPresent(announcementDetails
                        .get(AnnouncementDetails.Fields.hasAccident), criteriaBuilder, predicates);
            }
            if (!ObjectUtils.isEmpty(dto.getHasAccident()) && !dto.getHasAccident()) {
                addIsFalsePredicateIfPresent(announcementDetails
                        .get(AnnouncementDetails.Fields.hasAccident), criteriaBuilder, predicates);
            }

            if (!ObjectUtils.isEmpty(dto.getIsLoanAccepted()) && dto.getIsLoanAccepted()) {
                addIsTruePredicateIfPresent(announcementDetails
                        .get(AnnouncementDetails.Fields.isLoanAccepted), criteriaBuilder, predicates);
            }
            if (!ObjectUtils.isEmpty(dto.getIsLoanAccepted()) && !dto.getIsLoanAccepted()) {
                addIsFalsePredicateIfPresent(announcementDetails
                        .get(AnnouncementDetails.Fields.isLoanAccepted), criteriaBuilder, predicates);

            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        });


    }

//    default <Z, X> Join<Z, X> getOrCreateJoin(Root<Announcement> root, String attribute) {
//        for (Join<Announcement, ?> join : root.getJoins()) {
//            if (join.getAttribute().getName().equals(attribute)) {
//                return (Join<Z, X>) join;
//            }
//        }
//        return root.join(attribute, JoinType.INNER);
//    }

    private static ComparisonOperator getComparisonType(Object value1, Object value2) {

        if (!ObjectUtils.isEmpty(value1) && !ObjectUtils.isEmpty(value2))
            return BETWEEN;

        if (ObjectUtils.isEmpty(value1) && !ObjectUtils.isEmpty(value2))
            return LESS_THAN;

        return GREATER_THAN;
    }
}
