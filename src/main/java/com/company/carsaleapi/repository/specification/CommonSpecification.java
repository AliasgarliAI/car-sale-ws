package com.company.carsaleapi.repository.specification;

import com.company.carsaleapi.entity.FuelType;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface CommonSpecification {

    static void addLikePredicateIfPresent(String value,
                                          Root<String> attribute,
                                          CriteriaBuilder criteriaBuilder,
                                          List<Predicate> predicates) {

        Optional.ofNullable(value)
                .filter(val -> !val.isEmpty())
                .map(val -> criteriaBuilder.like(attribute, "%" + val + "%"))
                .ifPresent(predicates::add);
    }

    static <T> void addEqualPredicateIfPresent(T value,
                                               Path<String> attribute,
                                               CriteriaBuilder criteriaBuilder,
                                               List<Predicate> predicates) {
        if (value instanceof FuelType){
            System.out.println(value + " fuel type ");
        }
        Optional.ofNullable(value)
                .filter(ObjectUtils::isEmpty)
                .map(val -> criteriaBuilder.equal(attribute, val))
                .ifPresent(predicates::add);
    }

    static void addDatePredicateIfPresent(LocalDate startDate, LocalDate endDate,
                                          ComparisonOperator operator,
                                          Path<LocalDate> attribute,
                                          CriteriaBuilder criteriaBuilder,
                                          List<Predicate> predicates) {
        if (!ObjectUtils.isEmpty(startDate)) {
            switch (operator) {
                case EQUAL -> predicates.add(criteriaBuilder.equal(attribute, startDate));
                case LESS_THAN -> predicates.add(criteriaBuilder.lessThan(attribute, startDate));
                case GREATER_THAN -> predicates.add(criteriaBuilder.greaterThan(attribute, startDate));
                case BETWEEN -> {
                    if (!ObjectUtils.isEmpty(endDate))
                        predicates.add(criteriaBuilder.between(attribute, startDate, endDate));
                }
            }
        }
    }

    static void addIsTruePredicateIfPresent(Path<Boolean> attribute,
                                            CriteriaBuilder criteriaBuilder,
                                            List<Predicate> predicates) {
        predicates.add(criteriaBuilder.isTrue(attribute));
    }

    static void addIsFalsePredicateIfPresent(Path<Boolean> attribute,
                                             CriteriaBuilder criteriaBuilder,
                                             List<Predicate> predicates) {
        predicates.add(criteriaBuilder.isFalse(attribute));
    }

    static void addBigDecimalPredicateIfPresent(BigDecimal value1,
                                                BigDecimal value2,
                                                ComparisonOperator operator,
                                                Path<BigDecimal> attribute,
                                                CriteriaBuilder criteriaBuilder,
                                                List<Predicate> predicates) {
        if (!ObjectUtils.isEmpty(value1)) {
            switch (operator) {
                case EQUAL -> predicates.add(criteriaBuilder.equal(attribute, value1));
                case LESS_THAN -> predicates.add(criteriaBuilder.lessThan(attribute, value1));
                case GREATER_THAN -> predicates.add(criteriaBuilder.greaterThan(attribute, value1));
                case BETWEEN -> {
                    if (!ObjectUtils.isEmpty(value2))
                        predicates.add(criteriaBuilder.between(attribute, value1, value2));
                }
            }
        }
    }

    static void addIntegerPredicateIfPresent(Integer value1,
                                             Integer value2,
                                             ComparisonOperator operator,
                                             Path<Integer> attribute,
                                             CriteriaBuilder criteriaBuilder,
                                             List<Predicate> predicates) {
        switch (operator) {
            case EQUAL -> predicates.add(criteriaBuilder.equal(attribute, value1));
            case LESS_THAN -> predicates.add(criteriaBuilder.lessThan(attribute, value1));
            case GREATER_THAN -> predicates.add(criteriaBuilder.greaterThan(attribute, value1));
            case BETWEEN -> {
                if (!ObjectUtils.isEmpty(value2))
                    predicates.add(criteriaBuilder.between(attribute, value1, value2));
            }
        }
    }

    static void addLongPredicateIfPresent(
            Long value1,
            Long value2,
            ComparisonOperator operator,
            Path<Long> attribute,
            CriteriaBuilder criteriaBuilder,
            List<Predicate> predicates) {

        switch (operator) {
            case EQUAL -> predicates.add(criteriaBuilder.equal(attribute, value1));
            case LESS_THAN -> predicates.add(criteriaBuilder.lessThan(attribute, value1));
            case GREATER_THAN -> predicates.add(criteriaBuilder.greaterThan(attribute, value1));
            case BETWEEN -> {
                if (!ObjectUtils.isEmpty(value2)) {
                    predicates.add(criteriaBuilder.between(attribute, value1, value2));
                }
            }
        }
    }
}
