package com.company.carsaleapi.repository;

import com.company.carsaleapi.dto.UserWithAnnouncementsDto;
import com.company.carsaleapi.entity.Announcement;
import com.company.carsaleapi.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {


//    @Query("select u,a.name,ad.price,ad.priceCurrency from User u " +
//            "join Announcement a on u.id = a.user.id " +
//            "join AnnouncementDetails ad on ad.id = a.id " +
//            "where u.id = :userId " +
//            "limit :dataLimit offset :dataOffSet")
//    UserWithAnnouncementsDto findUserAnnouncements(@Param("userId") Long userId,
//                                                   @Param("limit") int dataLimit,
//                                                   @Param("offSet") int dataOffSet);


}
