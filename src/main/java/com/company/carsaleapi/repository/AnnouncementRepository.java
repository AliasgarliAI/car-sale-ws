package com.company.carsaleapi.repository;

import com.company.carsaleapi.entity.Announcement;
import com.company.carsaleapi.repository.specification.AnnouncementSearchSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnnouncementRepository extends JpaRepository<Announcement, Long>,
        JpaSpecificationExecutor<Announcement>, AnnouncementSearchSpecification {

    @EntityGraph(attributePaths = {"announcementDetails", "announcementDetails.city", "announcementDetails.brand",
            "announcementDetails.carModel", "announcementDetails.color", "announcementDetails.condition",
            "announcementDetails.vehicleSupply"})
//    @EntityGraph(value = "announcement.details", type = EntityGraph.EntityGraphType.LOAD)
    Page<Announcement> findAll(Pageable pageable);


//    @EntityGraph(attributePaths = {"announcementDetails"})
    @Query("select a from Announcement a left join fetch a.announcementDetails announcementDetails " +
            "where a.user.id = :userId")
    List<Announcement> getAnnouncementByUserId(Long userId, Pageable pageable);

    @EntityGraph(attributePaths = {"announcementDetails", "announcementDetails.city", "announcementDetails.brand",
            "announcementDetails.carModel", "announcementDetails.color", "announcementDetails.condition",
            "announcementDetails.vehicleSupply"})
    Page<Announcement> findAll( Specification<Announcement> spec, Pageable pageable);
}
