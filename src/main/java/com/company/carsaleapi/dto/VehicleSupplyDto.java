package com.company.carsaleapi.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class VehicleSupplyDto {

    private Long id;
    private boolean parkRadar;
    private boolean abs;
    private boolean sunRoof;
    private boolean rainSensor;
    private boolean sideCurtains;
    private boolean seatHeating;
    private boolean rearViewCamera;
    private boolean airConditioner;
    private boolean xenonHeadlight;
    private boolean seatVentilation;
    private boolean leatherSalon;
    private boolean centralLockDown;
    private boolean lightweightHardDrives;
}
