package com.company.carsaleapi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class AnnouncementDto {

    private Long id;
    private String name;
    private String description;
    private Long announcementCode;
    private UserDto user;
    private AnnouncementDetailsDto announcementDetails;

}
