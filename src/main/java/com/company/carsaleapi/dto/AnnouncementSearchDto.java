package com.company.carsaleapi.dto;

import com.company.carsaleapi.entity.FuelType;
import com.company.carsaleapi.entity.Gear;
import com.company.carsaleapi.entity.GearBox;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AnnouncementSearchDto {

    private FuelType fuelType;
    private GearBox gearBox;
    private Gear gear;
    private BigDecimal priceMin;
    private BigDecimal priceMax;
    private LocalDate manufactureDateMin;
    private LocalDate manufactureDateMax;
    private Boolean isLoanAccepted;
    private Boolean hasBarter;
    private Boolean hasAccident;
    private ColorDto color;
    private CityDto city;

}
