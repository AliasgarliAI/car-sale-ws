package com.company.carsaleapi.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserWithAnnouncementsDto {

    private Long id;
    private String fullName;
    private String mobileNumber;
    private String profilePicture;
    private List<UserAnnouncementsDto> announcements;

}
