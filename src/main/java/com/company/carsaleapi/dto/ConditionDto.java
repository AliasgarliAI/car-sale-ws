package com.company.carsaleapi.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.domain.PageRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ConditionDto {

    private int id;
    private boolean hasAccident;
    private boolean isColored;
    private boolean isForSpareParts;

}
