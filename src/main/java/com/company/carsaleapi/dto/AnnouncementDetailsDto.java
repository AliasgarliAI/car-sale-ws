package com.company.carsaleapi.dto;

import com.company.carsaleapi.entity.Currency;
import com.company.carsaleapi.entity.FuelType;
import com.company.carsaleapi.entity.Gear;
import com.company.carsaleapi.entity.GearBox;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class AnnouncementDetailsDto {

    private Long id;
    private Boolean hasAccident;
    private BigDecimal price;
    private String carMileage;
    private Gear gearType;
    private GearBox gearBoxType;
    private LocalDate manufactureDate;
    private Long engineCapacity;
    private Long enginePower;
    private int seatCount;
    private boolean hasBarter;
    private boolean isLoanAccepted;
    private String extraInformation;
    private String vinCode;
    private String address;
    private CityDto city;
    private ModelDto carModel;
    private VehicleSupplyDto vehicleSupply;
    private ColorDto color;
    private ConditionDto condition;
    private Currency priceCurrency;
    private FuelType fuelType;

}
