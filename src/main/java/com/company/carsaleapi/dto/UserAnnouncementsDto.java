package com.company.carsaleapi.dto;

import com.company.carsaleapi.entity.Currency;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserAnnouncementsDto {

    private Long announcementId;
    private String name;
    private BigDecimal price;
    private Currency currency;
}
