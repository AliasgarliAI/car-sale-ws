package com.company.carsaleapi.entity;

import lombok.Getter;

@Getter
public enum FuelType {
    Benzin,
    Dizel,
    Qaz,
    Hibrid,
    Elektro,
    Plug_in_hibrid

//    private final String value;
//
//    FuelType(String value){
//        this.value =value;
//    }
//
//    @Override
//    public String toString() {
//        return value;
//    }
}
