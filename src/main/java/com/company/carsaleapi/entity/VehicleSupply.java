package com.company.carsaleapi.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class VehicleSupply {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private boolean parkRadar;
    private boolean abs;
    private boolean sunRoof;
    private boolean rainSensor;
    private boolean sideCurtains;
    private boolean seatHeating;
    private boolean rearViewCamera;
    private boolean airConditioner;
    private boolean xenonHeadlight;
    private boolean seatVentilation;
    private boolean leatherSalon;
    private boolean centralLockDown;
    private boolean lightweightHardDrives;

}
