package com.company.carsaleapi.entity;

import lombok.Getter;

@Getter
public enum GearBox {

    Avtomat,
    Mexaniki,
    Reduktor,
    Robot,
    Variator

}
