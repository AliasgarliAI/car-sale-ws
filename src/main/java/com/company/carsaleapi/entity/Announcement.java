package com.company.carsaleapi.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import lombok.experimental.FieldNameConstants;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@FieldNameConstants
//@NamedEntityGraph(
//        name = "announcement.details",
//        attributeNodes = {
//                @NamedAttributeNode(value = "announcementDetails", subgraph = "announcementDetails"),
//                @NamedAttributeNode("user")
//        },
//        subgraphs = {
//                @NamedSubgraph(
//                        name = "announcementDetails",
//                        attributeNodes = {
//                                @NamedAttributeNode("city"),
//                                @NamedAttributeNode("color"),
//                                @NamedAttributeNode("brand"),
//                                @NamedAttributeNode("vehicleSupply"),
//                                @NamedAttributeNode("condition"),
//                                @NamedAttributeNode("carModel")
//                        })
//        }
//)
@Entity
public class Announcement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    private Long announcementCode;
    @Column(updatable = false)
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private AnnouncementDetails announcementDetails;

    @PrePersist
    public void prePersist() {
        createdAt = LocalDateTime.now();
    }

    @PostUpdate
    public void postUpdate() {
        updatedAt = LocalDateTime.now();
    }
}
