package com.company.carsaleapi.entity;

import lombok.Getter;

@Getter
public enum Currency {

    Azn,
    Usd,
    Eur

}
