package com.company.carsaleapi.entity;

import lombok.Getter;

@Getter
public enum Gear {

    Ön,
    Arxa,
    Tam;

}
