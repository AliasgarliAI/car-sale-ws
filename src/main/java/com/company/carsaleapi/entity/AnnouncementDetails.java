package com.company.carsaleapi.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedAttributeNode;
import jakarta.persistence.NamedEntityGraph;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@FieldNameConstants
public class AnnouncementDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Boolean hasAccident;
    private BigDecimal price;
    private String carMileage;
    private LocalDate manufactureDate;
    private Long engineCapacity;
    private Long enginePower;
    private int seatCount;
    private boolean hasBarter;
    private boolean isLoanAccepted;
    private String extraInformation;
    private String vinCode;
    private String address;
    @Enumerated(EnumType.STRING)
    private Gear gearType;
    @Enumerated(EnumType.STRING)
    private GearBox gearBoxType;
    @Enumerated(EnumType.STRING)
    private Currency priceCurrency;
    @Enumerated(EnumType.STRING)
    private FuelType fuelType;


    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private City city;

    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Brand brand;

    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Model carModel;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private VehicleSupply vehicleSupply;

    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Color color;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Condition condition;


}
