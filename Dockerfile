FROM openjdk:17-oracle
COPY build/libs/car-sale-api-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/car-sale-api-0.0.1-SNAPSHOT.jar"]
